University Relations - Enrollment Management
==================

**Theme Name:** University Relations - Enrollment Management

**Theme Description:** Theme for University Relations - Enrollment Management.

**Developers name(s):** Adam Glenn

**Stash repository URL:** [https://bitbucket.org/wvudigital/ur-university-relations-em/src](https://bitbucket.org/wvudigital/ur-university-relations-em/src)

**Dependencies necessary to work with this theme:** Sass.



## Gulp & Brand Patterns

**Requirements**
* [NodeJS](https://nodejs.org)

You will need to install Node 10.24.1.

  1. Download and install NodeJS from https://nodejs.org/en if you haven't already.
  1. Install Gulp globally by entering `npm install -g gulp` in your terminal.
  1. Navigate to your project's directory via terminal (something like `cd ~/Sites/cleanslate_themes/ur-university-relations-em`)
  1. Install node modules by typing `npm ci`
  1. Run Gulp by typing `gulp`.

**Note:** the `gulpfile.js` in its base form will only compile your Sass.
