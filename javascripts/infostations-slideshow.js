'use strict';

// Make semistandardjs linter ignore native methods
// https://standardjs.com/#i-use-a-library-that-pollutes-the-global-namespace-how-do-i-prevent-variable-is-not-defined-errors
/* global fetch, DOMParser, Splide */

// NOTE: XML Fetch Tutorial Video: https://www.youtube.com/watch?v=MDAWie2Sicc
// NOTE: Other feed URLs:
// http://infostations.wvu.edu/Webform/COVID19_HGraphics.xml // COVID Only
// http://infostations.wvu.edu/webform/systemwidehorizontal.xml // System wide horizontal
// https://virtual.infostations.wvu.edu/webform/DbliveContent.xml // NOTE: Start at 31 for COVID images
// NOTE: Jay Allen created virtual.infostations.wvu.edu to handle traffic overloading infostations.wvu.edu.

// IIFE
(function () {
  document.addEventListener('DOMContentLoaded', function () {
    var domId = '.js-fullscreen-image-slider';
    var errorMessage = '<h1 class="splide__error-message">Uh oh. Please add a valid feed URL via Page Properties to enable the slideshow.</h1>';
    var getSlideContainer = document.querySelector(domId);
    var url = getSlideContainer.dataset.feedUrl; // Grab feed url from data attribute
    if (!url) {
      getSlideContainer.outerHTML = errorMessage;
    }
    fetch(url)
      .then(checkError)
      .then(function (data) {
        var parser = new DOMParser();
        var xml = parser.parseFromString(data, 'application/xml');
        // console.log(xml); // Log the data to the console
        buildImagesList(xml);

        // SplideJS Slider:
        var splide = new Splide(domId, {
          autoplay: true,
          height: '100vh',
          interval: 20000, // How long to display each slide?
          lazyLoad: 'nearby', // or 'sequential'
          pagination: false,
          pauseOnHover: false, // Must be false
          pauseOnFocus: false, // Must be false
          perPage: 1, // Determine how many slides should be displayed per page.
          resetProgress: false,
          type: 'loop',
          width: '100vw'
        }).mount();

        // Display slide progress, eg: "1 of 10"
        splide.on('move', function () {
          // Get the slide number container
          var slideNumbers = document.querySelector('.splide__slide-number');

          // Make total number of slides equal to the length of the ul
          var slideNumberTotal = slideNumbers.querySelector('.splide__slide-number-total');
          slideNumberTotal.innerText = splide.length;

          // Update the current slide number
          var getSlideNumberCurrent = slideNumbers.querySelector('.splide__slide-number-current');
          var slideNumberCurrent = splide.index + 1;
          getSlideNumberCurrent.innerText = slideNumberCurrent;
        });

        // Toggle Play/Pause
        // NOTE: https://github.com/Splidejs/splide/issues/58
        var playPauseButton = document.querySelector('.splide__play-pause');
        if (playPauseButton) {
          var pausedClass = 'is-paused';

          // Remove the paused class and change the label to "Pause".
          splide.on('autoplay:play', function () {
            playPauseButton.classList.remove(pausedClass);
            playPauseButton.innerHTML = '<span class="splide__play-pause-icon splide__play-pause-icon--pause">❚❚</span>';
            playPauseButton.setAttribute('aria-label', 'Pause Autoplay');
          });

          // Add the paused class and change the label to "Play".
          splide.on('autoplay:pause', function () {
            playPauseButton.classList.add(pausedClass);
            playPauseButton.innerHTML = '<span class="splide__play-pause-icon">►</span>';
            playPauseButton.setAttribute('aria-label', 'Start Autoplay');
          });

          // Toggle play/pause when the button is clicked.
          splide.on('click', function () {
            var flag = 99;
            var Autoplay = splide.Components.Autoplay;
            if (playPauseButton.classList.contains(pausedClass)) {
              Autoplay.play(flag);
            } else {
              Autoplay.pause(flag);
            }
          }, playPauseButton);
        }

        // Put Play/Pause before the arrow controls in the DOM
        var getSplideStateControls = document.querySelector('.splide__state-controls');
        getSlideContainer.prepend(getSplideStateControls);
      })
      .catch(function (error) {
        getSlideContainer.outerHTML = errorMessage + '<p>More details: <code>' + error + '</code></p>';
        console.error(error);
      });

    function buildImagesList (dataSource) {
      var list = document.querySelector('.splide__list');
      var event = dataSource.querySelectorAll('event');
      for (var i = 0; i < event.length; i++) {
        // Create the list item:
        var li = document.createElement('li');
        li.classList.add('splide__slide');

        // Create the image:
        var img = document.createElement('img');
        // Get the image src and alt values:
        var imageUrl = event[i].querySelector('URL').firstChild.nodeValue; // NOTE: IE11 hates innerHTML/Text when dealing with XML, so we have to use this.
        var imageAlt = event[i].querySelector('Alt_Text').firstChild.nodeValue;

        // If there's a link available & it's valid, build:
        var webLink = event[i].querySelector('Web_Link').innerHTML; // IE11 doesn't get links. Suffer the consequences!
        if (isValidHttpUrl(webLink)) {
          // Shorten https://wwww.wvu.edu/return-to-campus to wvu.edu/return-to-campus:
          var webLinkNoProtocol = new URL(webLink).host + new URL(webLink).pathname;
          var webLinkContainer = document.createElement('div');
          webLinkContainer.classList.add('splide__slide-url-container');
          webLinkContainer.innerHTML = '<a class="splide__slide-url-link" href="' + webLink + '">' + webLinkNoProtocol + '</a>';
          li.appendChild(webLinkContainer);
        }

        // Set attributes on images and add them to the <ul>:
        img.setAttribute('data-splide-lazy', imageUrl);
        img.setAttribute('alt', imageAlt);
        li.appendChild(img);
        list.appendChild(li);
      }
    }

    // Check for valid https/http URL:
    // https://stackoverflow.com/a/43467144/908059
    // How to use: isValidHttpUrl('wvu.edu/return-to-campus');
    function isValidHttpUrl (string) {
      var url;
      try {
        url = new URL(string);
      } catch (_) {
        return false;
      }
      return url.protocol === 'http:' || url.protocol === 'https:';
    }

    // Handle Fetch API errrors more gracefully
    // https://learnwithparam.com/blog/how-to-handle-fetch-errors/
    function checkError (response) {
      if (response.status >= 200 && response.status <= 299) {
        return response.text(); // If using JSON, change this to .json()!
      } else {
        throw Error(response.statusText);
      }
    }
  });
})();
