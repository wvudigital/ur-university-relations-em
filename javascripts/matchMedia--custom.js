// Use match media to only load the HTML5 video for screens of 800+ px.
// Force the video to play via .play(); This is better than jQuery inserting
// an invalid attribute autoplay="autoplay".
// Add a play/pause button that users can click on
$(function(){
  if (matchMedia('only screen and (min-width: 800px)').matches) {
    $('video')[0].play(); // Force the video to play. Doing it this way circumvents an IE9 bug with the poster attribute.
    $('#js-video-loop__controls').removeClass('video-loop__playing').addClass('video-loop__playing');
  }
  $('#js-video-loop__controls').on('click', function(event){
    event.preventDefault();
    $video = $('video');
    if ($video[0].paused){
      $(this).toggleClass('video-loop__playing');
      $video[0].play();
    } else {
      $(this).toggleClass('video-loop__playing');
      $video[0].pause();
    }
  });
});
